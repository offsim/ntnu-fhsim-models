## NTNU FhSim Models

This is a library of ShipX based ship models for the use of NTNU Students. You may download the files, but you can only get support and training if you are a NTNU Student.

1. Checkout this repository in c:\work\ntnu-fhsim-models, the name of the folder must be **ntnu-fhsim-models** and the folder must be at the same level as [https://bitbucket.org/offsim/ntnu-fhsim-interface/]: **ntnu-fhsim-interface**

2. Setup OPUS_MODEL_PATH environment variable to the aforemoentioned folder
3. If you change the enviornment variables, either changing the place itself or only bat script, you need to restart netbeans 
4. To activate the various Fhsim Virtual Sea Trials, you need to turn the trials to true/false ![Alt text](vst_runs.png?raw=true "VST Runs")
5. Requirements for the shipx model: 
 1. **.re7** The lowest defined wave frequency needs to be above 0.1 rad/s, the highest frequency below 2𝜋𝜋 rad/s. Any frequencies below and above these values will be ignored. The recommended number of frequencies in the 0.25 - 2𝜋𝜋 rad/s range is at least 20. Their respective wave periods (i.e. reciprocal values) ought to be evenly distributed. Data for wave frequencies of 0.1, 0.125, 0.157, 0.180, 0.209, and 0.228 rad/s are mandatory. Missing mandatory frequencies will result in an error and render the respective load condition invalid. Coarse/unevenly defined frequencies will yield a warning
 2. **.re2** The wave directions in the file contain 0 and 180 degrees, as well as all angels in between with a 10-degree spacing. The velocity vector needs to contain 0. Any additionally defined velocities are ignored. A missing 0-velocity will yield an error and render the respective load condition invalid. Additional velocities will be logged.