
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.dates import DateFormatter
import matplotlib.dates as mdates
import datetime as dt
import pandas as pd
import math
import numpy as np

path = "C:\\Users\\pierre\\Dropbox (Personal)\\Doctorat\\Publication\\ICSOS2020\\Dp maneuver experiment\\gunnerus_21nov(2019-11-26 13.22.47)Variables.csv"
path_olex = "C:\\Users\\pierre\\Dropbox (Personal)\\Doctorat\\Publication\\ICSOS2020\\Dp maneuver experiment\\olex_course_speed.csv"
path_sway = "C:\\Users\\pierre\\Dropbox (Personal)\\Doctorat\\Publication\\ICSOS2020\\hemos data with olex gps\\SeapathGPSVbw\\all.csv"
path_zigzag = "C:\\work\\ntnu-fhsim-ship-interface\\ResultZigZag.csv"
path_vgt = "C:\\Users\\pierre\\Dropbox (Personal)\\Doctorat\\Publication\\ICSOS2020\\hemos data with olex gps\\gpsVtg\\all.csv"
path_swt = "C:\\Users\\pierre\\Dropbox (Personal)\\Doctorat\Publication\\ICSOS2020\\Dp maneuver experiment\\vannfart.csv"


# path_zigzag = "C:\\work\\fhsim_models\\RV_PK410_2_NTNU_Gunnerus\\dynmodel\\fhsim\\waterlines\\OUTPUT\\loadmaxdraft_crab_output.csv"


df_stw= pd.read_csv(path_swt)

print(df_stw.info())


utc_stw=df_stw.columns[0]
speed_stw=df_stw.columns[1]
df_stw['utc'] = df_stw[utc_stw].apply(np.int64)

df_stw["datetime"]=pd.to_datetime(df_stw["utc"], unit = 's')

df_crab_stw=df_stw.loc[df_stw["datetime"] > dt.datetime.strptime("2019-11-21 13:41:24",'%Y-%m-%d %H:%M:%S') ]
df_crab_stw=df_crab_stw.loc[df_crab_stw["datetime"] < dt.datetime.strptime("2019-11-21 13:43:14",'%Y-%m-%d %H:%M:%S')]
# print(df_crab_stw[speed_stw].describe())
# print(df_crab_stw.head())
# print(df_crab_stw.tail())
# exit()

df_sway = pd.read_csv(path_sway)
df_sway["datetime"]=pd.to_datetime(df_sway["time"],format='%Y-%m-%d %H:%M:%S.000')

df_sway=df_sway.loc[df_sway["datetime"] > dt.datetime.strptime("2019-11-21 13:38:54",'%Y-%m-%d %H:%M:%S') ]
df_sway=df_sway.loc[df_sway["datetime"] < dt.datetime.strptime("2019-11-21 13:48:54",'%Y-%m-%d %H:%M:%S')]

df_vgt = pd.read_csv(path_vgt,encoding="cp1252")
vgt_time=df_vgt.columns[0]
df_vgt["datetime"]=pd.to_datetime(df_vgt[vgt_time])

df = pd.read_csv(path,encoding="cp1252")
print(df.info())
exit()
df_olex  = pd.read_csv(path_olex,encoding="cp1252")
df_olex['utc'] = df_olex['utc'].apply(np.int64)
df_olex["datetime"]=pd.to_datetime(df_olex["utc"], unit = 's')

olex_speed = df_olex.columns[2]
olex_course = df_olex.columns[1]
lon_speed = df_sway.columns[1]
trans_speed = df_sway.columns[6]
trans_water_speed = df_sway.columns[7]
utc = df.columns[0]
lat=df.columns[13]
lon=df.columns[14]
gyro_measure=df.columns[7]
roll=df.columns[8]
Th2AngleFeedback=df.columns[23]
Th3AngleFeedback=df.columns[24]
rpm1 = df.columns[27]
rpm2= df.columns[25]
rpm3= df.columns[26]
dp_current_speed=df.columns[5]
dp_current_direction=df.columns[6]
gyro_measure=df.columns[7]
wind_speed=df.columns[11]
wind_dir=df.columns[12]
vgt_course_mag=df_vgt.columns[1]
vgt_course_true=df_vgt.columns[2]
vgt_speed_kt=df_vgt.columns[4]


# print(df[dp_current_direction].describe())
# print(df[gyro_measure].describe()) 
# print(df[wind_dir].describe()) 

df["diff_current"] = df[gyro_measure]-df[dp_current_direction]
df["diff_wind"] = df[gyro_measure]-df[wind_dir]

# print(df["diff_current"].describe()) 
print(df[["diff_wind","diff_current",wind_dir,gyro_measure,dp_current_direction]].describe()) 

# CONVERT FROM RADIANS TO DEGREES
df[lat]=df[lat].apply(lambda x: x *180 / math.pi)
df[lon]=df[lon].apply(lambda x: x *180 / math.pi)

df["datetime"]=pd.to_datetime(df[utc],format='%Y-%m-%d %H.%M.%S')


# exit()

################################## DP CRAB ##################################################
df_olex.index  = df_olex["datetime"]
df_olex = df_olex.resample('1S').mean()
df_olex["datetime"]=pd.to_datetime(df_olex["utc"], unit = 's')

# print(df_olex.info())
print(df_olex.head())


df_crab=df.loc[df["datetime"] > dt.datetime.strptime("2019-11-21 13:38:54",'%Y-%m-%d %H:%M:%S') ]
df_crab=df_crab.loc[df["datetime"] < dt.datetime.strptime("2019-11-21 13:46:54",'%Y-%m-%d %H:%M:%S')]
# print(df_crab.info())
df_crab.plot.scatter(x=lon,y=lat)
plt.title("DP Crab test: Positions lat/lon")
plt.show()

print("first: " + str(df_crab.iloc[0]["datetime"].timestamp()))

print("last: " + str(df_crab.iloc[-1]["datetime"].timestamp()))


fig, ax = plt.subplots()
ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=gyro_measure)
ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=dp_current_direction)

# ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=roll)
ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=Th2AngleFeedback)
ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=Th3AngleFeedback)
ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=rpm1)
ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=rpm3)
ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=rpm2)
plt.title("DP Crab test")
# df_crab.plot(x="datetime",y=gyro_measure)
# df_crab.plot(x="datetime",y=Th2AngleFeedback)
plt.show()


# 
f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)

ax1 = df_crab.plot(ax=ax1, kind='line', x='datetime', y=gyro_measure)
ax2 = df_crab.plot(ax=ax2, kind='line', x='datetime', y=dp_current_speed)
ax2 = df_vgt.plot(ax=ax2, kind='line', x='datetime', y=vgt_speed_kt)
ax2 = df_stw.plot(ax=ax2, kind='line', x='datetime', y=speed_stw)
ax2 = df_olex.plot(ax=ax2, kind='line', x='datetime', y=olex_speed)
ax = df_crab.plot(ax=ax1, kind='line', x='datetime', y=Th2AngleFeedback)
ax = df_crab.plot(ax=ax1, kind='line', x='datetime', y=Th3AngleFeedback)
ax = df_crab.plot(ax=ax1, kind='line', x='datetime', y=rpm1)
ax = df_crab.plot(ax=ax1, kind='line', x='datetime', y=rpm3)
ax = df_crab.plot(ax=ax1, kind='line', x='datetime', y=rpm2)
plt.title("DP Crab test")
plt.show()

######################### WHEN ACTUALLY DP-CRABBING ######################
df_crab_current=df_crab.loc[df_crab["datetime"] >= dt.datetime.strptime("2019-11-21 13:31:24",'%Y-%m-%d %H:%M:%S') ]
df_crab_current=df_crab_current.loc[df_crab_current["datetime"] < dt.datetime.strptime("2019-11-21 13:53:14",'%Y-%m-%d %H:%M:%S')]


df_crab=df_crab.loc[df_crab["datetime"] >= dt.datetime.strptime("2019-11-21 13:41:24",'%Y-%m-%d %H:%M:%S') ]
df_crab=df_crab.loc[df_crab["datetime"] < dt.datetime.strptime("2019-11-21 13:43:14",'%Y-%m-%d %H:%M:%S')]


#THIS is not not the same range, but there is a 30 second offset between hemos and olex time
df_crab_olex =df_olex.loc[df_olex["datetime"] > dt.datetime.strptime("2019-11-21 13:41:24",'%Y-%m-%d %H:%M:%S') ]
df_crab_olex=df_crab_olex.loc[df_crab_olex["datetime"] < dt.datetime.strptime("2019-11-21 13:43:14",'%Y-%m-%d %H:%M:%S')]

df_vgt[vgt_course_mag]=df_vgt[vgt_course_mag].apply(lambda x: x if x < 270 else (x -360))
df_vgt[vgt_course_true]=df_vgt[vgt_course_true].apply(lambda x: x if x < 270 else (x -360))
df_crab_vgt = df_vgt.loc[df_vgt["datetime"] > dt.datetime.strptime("2019-11-21 13:41:24",'%Y-%m-%d %H:%M:%S') ]
df_crab_vgt = df_crab_vgt.loc[df_crab_vgt["datetime"]  < dt.datetime.strptime("2019-11-21 13:43:14",'%Y-%m-%d %H:%M:%S')]


# df_crab_olex = df_crab_olex[df_crab_olex.index % 2 != 0] 
# df_crab_olex.reset_index()
# print(df_crab_olex.info())
print(df_crab_olex.head())
print(df_crab_olex.tail())


angle = df_crab_current[dp_current_direction]
print(angle.head())
t = mpl.markers.MarkerStyle(marker='>')
print(angle.values.tolist())
t._transform = t.get_transform().rotate_deg(90)



print("##################### DB CRAB VALUES ################")

print(df_crab[[rpm1,Th2AngleFeedback,rpm2,Th3AngleFeedback, rpm3, gyro_measure,wind_dir,wind_speed, dp_current_direction,dp_current_speed]].describe())
print(df_crab_olex[olex_speed].describe())
# print(df_crab_vgt.info())
# print(df_crab_vgt.head())
print(df_crab_vgt[[vgt_course_mag,vgt_speed_kt]].describe())
# exit()
f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
f.suptitle("DP Crab test")
ax1 = df_crab.plot(ax=ax1, kind='line', x='datetime', y=gyro_measure)
ax1 = df_crab.plot(ax=ax1, kind='line', x='datetime', y=dp_current_direction)
ax1 = df_crab_vgt.plot(ax=ax1, kind='line', x='datetime', y=vgt_course_mag)
ax1 = df_crab.plot(ax=ax1, kind='line', x='datetime', y=rpm1)
ax1 = df_crab.plot(ax=ax1, kind='line', x='datetime', y=rpm3)
ax1 = df_crab.plot(ax=ax1, kind='line', x='datetime', y=rpm2)
# ax2 = df_crab.plot(ax=ax2, kind='line', x='datetime', y=dp_speed)
ax2 = df_crab_olex.plot(ax=ax2, kind='line', x='datetime', y=olex_speed)
ax2 = df_crab_vgt.plot(ax=ax2, kind='line', x='datetime', y=vgt_speed_kt)
ax2 = df_crab_stw.plot(ax=ax2, kind='line', x='datetime', y=speed_stw)
ax2 = df_sway.plot(ax=ax2, kind='line', x='datetime', y=trans_speed)
ax2 = df_sway.plot(ax=ax2, kind='line', x='datetime', y=trans_water_speed)
ax2 = df_sway.plot(ax=ax2, kind='line', x='datetime', y=lon_speed)
plt.show()

fig, ax = plt.subplots()
ax = df_crab_olex.plot(ax=ax, kind='line', x='datetime', y=olex_speed)
plt.title("OLEX SPEED during DP-Crab")
plt.show()


f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
f.suptitle("DP Crab test CURRENT")
ax1 = df_crab_current.plot(ax=ax1, kind='line', x='datetime', y=gyro_measure)
ax1 = df_crab_current.plot(ax=ax1, kind='line', x='datetime', y=rpm1)
ax1 = df_crab_current.plot(ax=ax1, kind='line', x='datetime', y=rpm3)
ax1 = df_crab_current.plot(ax=ax1, kind='line', x='datetime', y=rpm2)
# ax2 = df_crab.plot(ax=ax2, kind='line', x='datetime', y=dp_speed)
ax2 = df_olex.plot(ax=ax2, kind='line', x='datetime', y=olex_speed)
ax2 = df_crab_vgt.plot(ax=ax2, kind='line', x='datetime', y=vgt_speed_kt)
ax2 = df_crab_stw.plot(ax=ax2, kind='line', x='datetime', y=speed_stw)
ax2 = df_sway.plot(ax=ax2, kind='line', x='datetime', y=trans_speed)
ax2 = df_sway.plot(ax=ax2, kind='line', x='datetime', y=trans_water_speed)
ax2 = df_sway.plot(ax=ax2, kind='line', x='datetime', y=lon_speed)
plt.show()

fig, ax = plt.subplots()
ax = df_crab_olex.plot(ax=ax, kind='line', x='datetime', y=olex_speed)
plt.title("OLEX SPEED during DP-Crab")
plt.show()



exit()




##############DP PIROUETTE###########################
df_pirouette=df.loc[df["datetime"] > dt.datetime.strptime("2019-11-21 13:47:43",'%Y-%m-%d %H:%M:%S') ]
df_pirouette=df_pirouette.loc[df_pirouette["datetime"] < dt.datetime.strptime("2019-11-21 13:48:54",'%Y-%m-%d %H:%M:%S')]
df_pirouette.plot.scatter(x=lon,y=lat)
plt.title("DP Pirouette test: Positions lat/lon")
plt.show()

# fig, ax = plt.subplots()
# ax = df_pirouette.plot(ax=ax, kind='line', x='datetime', y=gyro_measure)
# plt.title("DP Pirouette heading")
# plt.show()
df_pirouette[gyro_measure]=df_pirouette[gyro_measure].apply(lambda x: x if x > 270 else (x +360))
df_pirouette[gyro_measure]=df_pirouette[gyro_measure]-180

yaw_speed_pir = 'yaw speed [deg/sec]'
df_pirouette[yaw_speed_pir] = df_pirouette[gyro_measure].diff()

print(df_pirouette[yaw_speed_pir].describe())

f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
# print(df_pirouette.info())

ax1 = df_pirouette.plot(ax=ax1, kind='line', x='datetime', y=gyro_measure)
ax1 = df_pirouette.plot(ax=ax1, kind='line', x='datetime', y=yaw_speed_pir)

ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=Th2AngleFeedback)
ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=Th3AngleFeedback)
ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=rpm3)
ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=rpm2)
ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=rpm1)


plt.title("DP Pirouette speed and allocation")
plt.show()




# ##############YAW DECAY###########################
# df_pirouette=df_pirouette.loc[df_pirouette["datetime"] > dt.datetime.strptime("2019-11-21 13:49:20",'%Y-%m-%d %H:%M:%S') ]
# df_pirouette=df_pirouette.loc[df_pirouette["datetime"] < dt.datetime.strptime("2019-11-21 13:50:54",'%Y-%m-%d %H:%M:%S')]


###############################VALUES#####################################
print(df_pirouette[[rpm1,Th2AngleFeedback,rpm2,Th3AngleFeedback, rpm3, yaw_speed_pir, gyro_measure,wind_dir,wind_speed]].describe())



f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)

ax1 = df_pirouette.plot(ax=ax1, kind='line', x='datetime', y=gyro_measure)
ax1 = df_pirouette.plot(ax=ax1, kind='line', x='datetime', y=yaw_speed_pir)

ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=Th2AngleFeedback)
ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=Th3AngleFeedback)
ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=rpm3)
ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=rpm2)
ax = df_pirouette.plot(ax=ax2, kind='line', x='datetime', y=rpm1)


plt.title("Yaw decay: NOT PROPERLY PERFORMED ")
plt.show()

#########################FULL SPEED############################################
df_FULL=df.loc[df["datetime"] >= dt.datetime.strptime("2019-11-21 13:11:24",'%Y-%m-%d %H:%M:%S') ]
df_FULL=df_FULL.loc[df_FULL["datetime"] < dt.datetime.strptime("2019-11-21 13:27:14",'%Y-%m-%d %H:%M:%S')]
df_FULL[gyro_measure]=df_FULL[gyro_measure].apply(lambda x: x if x <= 180 else (x -360))

df_full_olex =df_olex.loc[df_olex["datetime"] > dt.datetime.strptime("2019-11-21 13:11:24",'%Y-%m-%d %H:%M:%S') ]
df_full_olex=df_full_olex.loc[df_full_olex["datetime"] < dt.datetime.strptime("2019-11-21 13:27:14",'%Y-%m-%d %H:%M:%S')]




f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
f.suptitle("FULL SPEED test")
ax1 = df_FULL.plot(ax=ax1, kind='line', x='datetime', y=gyro_measure)
ax1 = df_FULL.plot(ax=ax1, kind='line', x='datetime', y=rpm1)
ax1 = df_FULL.plot(ax=ax1, kind='line', x='datetime', y=rpm3)
ax1 = df_FULL.plot(ax=ax1, kind='line', x='datetime', y=rpm2)
ax2 = df_full_olex.plot(ax=ax2, kind='line', x='datetime', y=olex_speed)
plt.show()



print("#####################FULL SPEED TEST VALUES################")
df_FULL=df.loc[df["datetime"] >= dt.datetime.strptime("2019-11-21 13:15:29",'%Y-%m-%d %H:%M:%S') ]
df_FULL=df_FULL.loc[df_FULL["datetime"] < dt.datetime.strptime("2019-11-21 13:19:20",'%Y-%m-%d %H:%M:%S')]
df_FULL[gyro_measure]=df_FULL[gyro_measure].apply(lambda x: x if x <= 180 else (x -360))

df_full_olex =df_olex.loc[df_olex["datetime"] > dt.datetime.strptime("2019-11-21 13:15:29",'%Y-%m-%d %H:%M:%S') ]
df_full_olex=df_full_olex.loc[df_full_olex["datetime"] < dt.datetime.strptime("2019-11-21 13:19:20",'%Y-%m-%d %H:%M:%S')]

print(df_crab[[rpm1,Th2AngleFeedback,rpm2,Th3AngleFeedback, rpm3, gyro_measure,wind_dir,wind_speed]].describe())
print(df_crab_olex[olex_speed].describe())


# colEast1= df_crab.columns[33]
# colWest1= df_crab.columns[35]

# df_crab.plot.scatter(x=colEast1,y=colWest1)
# plt.show()
print(df_olex.head())
#problem with sync
df_olex["utc"] = df_olex["utc"].apply(lambda x: x - 30)
print(df_olex.head())
df_olex = df_olex.loc[df_olex["utc"] > 1574343535.0 ]
df_olex = df_olex.loc[df_olex["utc"] < 1574344133.0 ]

fig, ax = plt.subplots()
ax = df_olex.plot(ax=ax, kind='line', x='datetime', y=olex_speed)
ax = df_crab.plot(ax=ax, kind='line', x='datetime', y=dp_current_speed)

plt.title("DP Crab \n Olex speed vs. DP Estimator [knots]")
# df_crab.plot(x="datetime",y=gyro_measure)
# df_crab.plot(x="datetime",y=Th2AngleFeedback)

plt.show()


df_olex['course_rectified'] = df_olex[olex_course].apply(lambda x: x if x <= 180 else (x -360))

fig, ax = plt.subplots()
ax = df_olex.plot(ax=ax, kind='line', x='datetime', y='course_rectified')
plt.title("OLEX Heading during DP-Crab")
plt.show()

fig, ax = plt.subplots()
f.suptitle("DP Crab \n Olex speed vs.SeaPath VbW [knots]")
ax = df_olex.plot(ax=ax, kind='line', x='datetime', y=olex_speed)
ax = df_sway.plot(ax=ax, kind='line', x='datetime', y=trans_speed)
ax = df_sway.plot(ax=ax, kind='line', x='datetime', y=trans_water_speed)
ax = df_sway.plot(ax=ax, kind='line', x='datetime', y=lon_speed)
plt.show()

exit()

# zig zag test 80% RPM, 20 degrees rudder, 20 heading limit
df_zig_zag=df.loc[df["datetime"] > dt.datetime.strptime("2019-11-21 11:53:54",'%Y-%m-%d %H:%M:%S') ]
df_zig_zag=df_zig_zag.loc[df["datetime"] < dt.datetime.strptime("2019-11-21 11:56:54",'%Y-%m-%d %H:%M:%S')]
# df_zig_zag[gyro_measure]=df_zig_zag[gyro_measure]-720

fig, ax = plt.subplots()
ax = df_zig_zag.plot(ax=ax, kind='line', x='datetime', y=gyro_measure)
ax = df_zig_zag.plot(ax=ax, kind='line', x='datetime', y=roll)
ax = df_zig_zag.plot(ax=ax, kind='line', x='datetime', y=Th2AngleFeedback)
ax = df_zig_zag.plot(ax=ax, kind='line', x='datetime', y=rpm3)
ax = df_zig_zag.plot(ax=ax, kind='line', x='datetime', y=rpm2)

# df_zig_zag.plot(x="datetime",y=gyro_measure)
# df_zig_zag.plot(x="datetime",y=Th2AngleFeedback)
plt.title("zig zag test")
plt.show()

df_lat_lon=df[[lat,lon]]
print(df_lat_lon.info())



df_zz = pd.read_csv(path_zigzag,sep=';', skiprows=1)
df_zz.rename(columns={ df_zz.columns[0]: "time [s]" }, inplace = True)


# print(df_zz.columns.tolist())
# print(df_zz.info())
# print(df_zz.head())


df_zz.plot.scatter(x='PositionNED_1',y='PositionNED_0')
plt.title("Zig Zag Test")
plt.show()

fig, ax = plt.subplots()
df_zz['OmegaNED_2.3']=df_zz['OmegaNED_2.3']*180/math.pi
df_zz['CommandedAngle_0']=df_zz['CommandedAngle_0']*180/math.pi
df_zz['CommandedAngle_0.1']=df_zz['CommandedAngle_0.1']*180/math.pi
ax = df_zz.plot(ax=ax, kind='line', x='time [s]', y='OmegaNED_2.3')
ax = df_zz.plot(ax=ax, kind='line', x='time [s]', y='CommandedAngle_0')
ax = df_zz.plot(ax=ax, kind='line', x='time [s]', y='CommandedAngle_0.1')

plt.title("Zig Zag Test")
plt.show()




