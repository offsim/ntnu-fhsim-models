import numpy as np


# class Inertia:
#     def __init__(self, mass, Ixx=1, Iyy=1, Izz=1, Ixy=0, Iyz=0, Izx=0, centre_of_mass=np.array(shape=(3,))):
#         self.mass = mass
#         self.Ixx = Ixx
#         self.Iyy = Iyy
#         self.Izz = Izz
#         self.Ixy = Ixy
#         self.Iyz = Iyz
#         self.Izx = Izx
#         self.centre_of_mass = centre_of_mass

#     def get_inertia3x3_at(self, position):
#         r = position - self.centre_of_mass
#         inertia = np.array([self.Ixx, self.Ixy, self.Izx],
#                            [self.Ixy, self.Iyy, self.Izx],
#                            [self.Izx, self.Ixy, self.Izz])
#         inertia = inertia + mass * \
#             (np.diag(shape=(3, 3)) * np.dot(r, r) - np.outer(r, r))
#         return inertia

#     def get_inertia6x6_at(self, position):
#         r = position - self.centre_of_mass
#         r_smtrx = np.array(
#             [[0, -r(2), r(1)], [r[2], 0, -r(0)], [-r(1), r(0), 0]])
#         return np.array([self.mass * np.diag(shape=(3, 3)), -self.mass * r_smtrx], [self.mass * r_smtrx, self.get_inertia3x3_at(r)])

# class Damping:
#     def __init__(self, damping6x6=np.array(shape=(6, 6))):
#         self.damping6x6 = damping6x6

#     def get_damping6x6_at_cog(self):
#         return 0  # todo


class HydroData:
    def __init__(self):
        self.lpp = None
        self.breath = None
        self.draught = None
        self.sinkage = None
        self.trim = None
        self.cb = 1
        self.cw = 1
        self.cp = 1
        self.cm = 1
        self.r44 = None
        self.r55 = None
        self.r66 = None
        self.r46 = None
        self.water_depth = None
        self.density_of_water = None
        self.gravity = None
        self.cog = None
        self.inertia = None
        self.wave_frequency = None
        self.wave_direction = None
        self.mesh_based_displacement = None
        self.mass_based_displacement = None
        self.centre_of_buoyancy = None
        self.water_plane_area = None
        self.gmx = None
        self.gmy = None
        self.stiffness = None
        self.added_mass = None
        self.damping = None
        self.wave_force = None
        self.motion_rao = None
        self.viscous_damping = None
        self.added_resistance = None
