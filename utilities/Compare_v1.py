# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

"""
This script compare the results from ShipX (.re1 file) and from Virtual Sea Trials application (.csv file)
The tests have to be launch for the same periods the same directions
"""

import numpy
import cmath
import matplotlib.pyplot as plt


def createMatrix(value, nbRow, nbColumn):
    matrix = []
    row = []
    for i in range(0, nbRow):
        for j in range(0, nbColumn):
            row.append(value)
        matrix.append(row)
        row = []
    return(matrix)

def readShipXFile(filePath):
    file = open(filePath, "r")
    lines = file.readlines()
    file.close()
    
    arr = []
    for element in lines[9].split(' '):
        if element != '':
            arr.append(int(element))
            
    #ampWave = arr[0]
    nbDirections = arr[1]
    nbPeriods = arr[2]
    nbDof = arr[3]
    
    results = []
    values = []
    dof = []
    arr = []
    print("number of directions" ,nbDirections)
    print("number of periods" ,nbPeriods)
    
    for i in range(0, nbDirections):
        direction = float(lines[11+i*(nbPeriods+nbDof*nbPeriods+1)])
        for j in range(0, nbPeriods):
            period = 2*numpy.pi/float(lines[12+i*(nbPeriods+nbDof*nbPeriods+1)+j*(nbDof+1)])
            for k in range(0, nbDof):
                for element in lines[13+i*(nbPeriods+nbDof*nbPeriods+1)+j*(nbDof+1)+k].split(' '):
                    if element != '':
                        arr.append(float(element))
                z = complex(arr[1], arr[2])
                amp, phase = cmath.polar(z)
                dof.append([amp, phase*180/numpy.pi])
                arr = []
            values.append([period, dof])
            dof = []
        results.append([direction, values])
        values = []
        
    return(results)

def readVSTFile(filePath):
    file = open(filePath, "r")
    header = file.readline()
    lines = file.readlines()
    file.close()
    
    arr = []
    results = []
    values = []
    previousDirection = lines[0].split(',')[0]
    for i in range(0, len(lines)):
        currentDirection = lines[i].split(',')[0]
        if currentDirection != previousDirection :
            results.append([previousDirection, values])
            previousDirection = currentDirection
            values = []
            for element in lines[i].split(','):
                arr.append(float(element))
            values.append([arr[1], [[arr[2], None], [arr[3], None], [arr[4],arr[8]], [arr[5],arr[9]], [arr[6], arr[10]], [arr[7], None]]])
            arr = []
        else:
            for element in lines[i].split(','):
                arr.append(float(element))
            values.append([arr[1], [[arr[2], None], [arr[3], None], [arr[4],arr[8]], [arr[5],arr[9]], [arr[6], arr[10]], [arr[7], None]]])
            arr = []
    results.append([previousDirection, values])
    
    return(results)


def createChartDataset(resultsArray):
    nbFilesToCompare = len(resultsArray)
    nbDirections = len(resultsArray[0])
    nbPeriods = len(resultsArray[0][0][1])
    Yheave_amp = []
    Yroll_amp = []
    Ypitch_amp = []
    Yheave_phase = []
    Yroll_phase = []
    Ypitch_phase = []
    Xarray = []
    dataset = createMatrix(0, nbFilesToCompare, nbDirections)
    
    #Check if data can be compare from the files:
    #Same directions
    print("{} files to compare".format(nbFilesToCompare))
    for fileNb in range(0, nbFilesToCompare):
        nbDirections = len(resultsArray[fileNb])
        print("For File #{}, {} directions found:".format(fileNb, nbDirections))
        for direction in range(0, nbDirections):
            print("{} deg".format(resultsArray[fileNb][direction][0]))
    print("!!! Check if directions are the same !!!")
    
    for fileNb in range(0, nbFilesToCompare):
        for direction in range(0, nbDirections):
            for period in range(0, nbPeriods):
                values = resultsArray[fileNb][direction][1][period]
                
                Xarray.append(values[0])
                Yheave_amp.append(values[1][2][0])
                Yroll_amp.append(values[1][3][0])
                Ypitch_amp.append(values[1][4][0])
                Yheave_phase.append(values[1][2][1])
                Yroll_phase.append(values[1][3][1])
                Ypitch_phase.append(values[1][4][1])
                
            dataset[fileNb][direction] = [Xarray, [Yheave_amp, Yroll_amp, Ypitch_amp, Yheave_phase, Yroll_phase, Ypitch_phase]]

            Yheave_amp = []
            Yroll_amp = []
            Ypitch_amp = []
            Yheave_phase = []
            Yroll_phase = []
            Ypitch_phase = []
            Xarray = []
    return(dataset)
#
#resLow = readShipXFile("C:\\Users\\hadrienc\\Desktop\\Fathom\\RAO_results\\Compare_v1\\inputLow.re1")
#resHigh = readShipXFile("C:\\Users\\hadrienc\\Desktop\\Fathom\\RAO_results\\Compare_v1\\inputHigh.re1")
#resVST = readVSTFile("C:\\Users\\hadrienc\\Desktop\\Fathom\\RAO_results\\Compare_v1\\RAOTrial.csv")

resLow = readShipXFile("C:\\work\\ntnu-fhsim-models\\RV_PK410_2_NTNU_Gunnerus\\dynmodel\\fhsim\\waterlines\\loadmaxdraft\\input.re1")
#resHigh = readShipXFile("inputHigh.re1")
#resVST = readVSTFile("RAOTrial.csv")

dataset = createChartDataset([resLow])

for dir in dataset:
    print(len(dir))
    print(len(dir[0]))
    print(len(dir[1]))
    print(len(dir[0][1]))
    print(len(dir[1][1]))
    print("----")
    for der in dir[1][1]:
        print(der)



# dataset = createChartDataset([resLow, resLow, resLow])

# for i in range(0, len(dataset[0])):
#     for j in range(0, len(dataset[0][0][1])):
#         plt.figure((i+1)*(j+1), [10, 7])
#         if j == 0:
#             plt.title("Direction {} - Heave amplitude".format(resLow[i][0]))
#         elif j == 1:
#             plt.title("Direction {} - Roll amplitude".format(resLow[i][0]))
#         elif j == 2:
#             plt.title("Direction {} - Pitch amplitude".format(resLow[i][0]))
#         elif j == 3:
#             plt.title("Direction {} - Heave phase".format(resLow[i][0]))
#         elif j == 4:
#             plt.title("Direction {} - Roll phase".format(resLow[i][0]))
#         elif j == 5:
#             plt.title("Direction {} - Pitch phase".format(resLow[i][0]))
#         plt.plot(dataset[0][i][0], dataset[0][i][1][j], label="ShipXLow")
#         plt.plot(dataset[1][i][0], dataset[1][i][1][j], label="ShipXHigh")
#         plt.plot(dataset[2][i][0], dataset[2][i][1][j], label="VST")
#         plt.legend(loc='best', fancybox=True)
#         plt.grid(which='major', color='Black', linestyle='--')
#       #  plt.savefig("Direction_{}-{}.png".format(resLow[i][0], j))
#     plt.show()