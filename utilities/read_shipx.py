import re
import numpy as np
import hydro_data as hd

float_re = r"[+-]?\d+\.\d*[eE]?[+-]?\d*"
float_and_int_re = r"[+-]?\d+\.?\d*[eE]?[+-]?\d*"
int_re = r"[+-]?\d+"

def parse_single_line_single_float(line):
    match = re.search(float_re, line)
    if match:
        value = float(match.group(0))
        return value


def parse_single_line_multiple_floats(line):
    match_list = re.findall(float_re, line)
    if match_list:
        values = [float(item) for item in match_list]
        return values

def parse_single_line_multiple_ints(line):
    match_list = re.findall(int_re, line)
    if match_list:
        values = [int(item) for item in match_list]
        return values

def parse_finite_lines_multiple_floats(lines):
    match_list = []
    for item in lines:
        match_list = match_list + re.findall(float_re, item)
    return [float(item) for item in match_list]


def get_hydro_data(file_name):
    hyd_lines = open(file_name + ".hyd", "r").readlines()
    re2_lines = open(file_name + ".re2", "r").readlines()
    re7_lines = open(file_name + ".re7", "r").readlines()
    re8_lines = open(file_name + ".re8", "r").readlines()
    data = hd.HydroData()
    
    with open(file_name + ".hyd", "r") as f:
        for i, line in enumerate(f):
            if data.mass_based_displacement is None and data.mesh_based_displacement is None and "Displacement" in line:
                delta = parse_single_line_multiple_floats(line)
                data.mass_based_displacement = delta[1]
                data.mesh_based_displacement = delta[1]

            if data.water_plane_area is None and "Length between perpendiculars" in line:
                lpp = parse_single_line_single_float(line)
                breadth = parse_single_line_single_float(hyd_lines[i + 1])
                coeff_water_plane = parse_single_line_multiple_floats(
                    hyd_lines[i + 17])[1]
                data.water_plane_area = lpp*breadth*coeff_water_plane

            if data.cog is None and "Vertical center of gravity" in line:
                cog_z = parse_single_line_single_float(line)
                cog_x = parse_single_line_multiple_floats(hyd_lines[i + 2])[1]
                data.cog = [cog_x, 0, cog_z]
                continue

            if data.centre_of_buoyancy is None and "Vertical center of bouyancy" in line:
                cob_z = parse_single_line_single_float(line)
                cob_x = parse_single_line_single_float(hyd_lines[i + 2])
                data.centre_of_buoyancy = [cob_x, 0, cob_z]
                continue

            if data.gmx is None and "Longitudinal metacentric height" in line:
                data.gmx = parse_single_line_single_float(line)
                continue

            if data.gmy is None and "Transverse metacentric height" in line:
                data.gmy = parse_single_line_single_float(line)
                continue
    with open(file_name + ".re7", "r") as f:
        start_idx = 0
        for i, line in enumerate(f):
            if "ShipX exported data" in line:
                start_idx = i
                break
        match_list = parse_finite_lines_multiple_floats(
            [re7_lines[start_idx + 1], re7_lines[start_idx + 2], re7_lines[start_idx + 3]])
        data.density_of_water = match_list[0]
        data.gravity = match_list[1]
        match_list = parse_single_line_multiple_ints(re7_lines[start_idx + 5])
        num_heading = match_list[1]
        num_frequency = match_list[2]
        data.viscous_damping = np.empty(shape = [num_heading, num_frequency, 3], dtype = float)
        inertia = parse_finite_lines_multiple_floats([re7_lines[start_idx + 6],
                                                      re7_lines[start_idx + 7], 
                                                      re7_lines[start_idx + 8],
                                                      re7_lines[start_idx + 9],
                                                      re7_lines[start_idx + 10],
                                                      re7_lines[start_idx + 11]])
        inertia = np.array(inertia, dtype=float).reshape(6,6)
        data.inertia = inertia[3:6,3:6]
        data.wave_direction = np.empty(shape=[num_heading,])
        data.wave_frequency = np.empty(shape=[num_frequency,])
        data.added_mass = np.zeros(shape = [num_frequency,6,6])
        data.damping = np.zeros(shape = [num_frequency,6,6])
        start_idx = start_idx + 13
        abc_size = 36
        all_frequencies_found = False
        for i in range(num_heading):
            match_list = parse_single_line_multiple_floats(re7_lines[start_idx])
            data.wave_direction[i] = match_list[0]
            start_idx += 1
            for j in range(num_frequency):
                match_list1 = parse_single_line_multiple_floats(re7_lines[start_idx])
                if i == 0:
                    data.wave_frequency[j] = match_list1[0]
                start_idx += 1
                matrix_list = parse_finite_lines_multiple_floats([re7_lines[start_idx + k] for k in range(abc_size)])
                matrix = np.array(matrix_list, dtype=float).reshape(1, abc_size, 6)
                
                data.added_mass[data.wave_frequency.size - 1, : ,:] = matrix[:, 0:6, 0:6]
                data.damping[data.wave_frequency.size - 1, : ,:] = matrix[:, 12:18, 0:6]
                if data.stiffness is None:
                    data.stiffness = matrix[0,24:30, 0:6]
                start_idx += abc_size + 1
                data.viscous_damping[i, j, : ] = np.array(parse_single_line_multiple_floats(re7_lines[start_idx - 1]), dtype = float)
    with open(file_name + ".re8", "r") as f:
        start_idx = 0
        for i, line in enumerate(f):
            if "ShipX exported data" in line:
                start_idx = i
                break
        match_list = parse_single_line_multiple_ints(re8_lines[start_idx + 4])
        num_heading = match_list[1]
        num_frequency = match_list[2]
        start_idx = start_idx + 8
        data.wave_force = np.empty(shape = [num_heading, num_frequency, 6], dtype=complex)
        for i in range(num_heading):
            for j in range(num_frequency):
                matrix_list = parse_finite_lines_multiple_floats([re8_lines[start_idx + k] for k in range(6)])
                matrix = np.array(matrix_list, dtype=float).reshape(6,8)
                for k in range(6):
                    data.wave_force[i,j,k] = matrix[k, 0] + matrix[k,1] * 1j
                start_idx += 7
            start_idx += 1
    with open(file_name + ".re2", "r") as f:
        start_idx = 0
        for i, line in enumerate(f):
            if "ShipX exported data" in line:
                start_idx = i
                break
        match_list = parse_single_line_multiple_ints(re8_lines[start_idx + 4])
        num_heading = match_list[1]
        num_frequency = match_list[2]
        start_idx = start_idx + 5
        data.added_resistance = np.empty(shape = [num_heading, num_frequency, 9], dtype=float)
        for i in range(num_heading):
            for j in range(num_frequency):
                foo =  np.array(parse_single_line_multiple_floats(re2_lines[start_idx + i * (num_frequency + 1) + j]), dtype = float)
                data.added_resistance[i,j,:] = foo[1:10]
    return data
